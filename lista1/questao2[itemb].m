# f(x) = sin(x) =>  f'(x) = cos(x)
x0 = 1.2;
f0 = sin(x0);
fp = cos(x0);

h = 10.^(-1:-1:-20);
fp_aprox = (sin(x0 + h) - sin(x0 - h)) ./ (2 .* h);

err = abs(fp - fp_aprox);

[h; err]'

#{
COMENTÁRIOS:

Os erros absolutos da tabela desse exercício são menores que os erros absolutos
da tabela do exemplo 1.2, mas vale notar que, a partir de h = 1e-07, esses erros
absolutos começam a aumentar consideravelmente o seu valor. Uma hipótese para esse
fato seria os erros de arredondamento acumulados. 

#}
