# f(x) = sin(x) => f'(x) = cos(x) => f''(x) = -sin(x) => f'''(x) = -cos(x)
x0 = 1.2;
f0 = sin(x0);
fp = cos(x0);

i = -20:0.5:0;
h = 10.^i
fp_aprox = (sin(x0 + h) - sin(x0 - h)) ./ (2 .* h);

err = abs(fp - fp_aprox);

# |f'(x0) - (f(x0 + h) - f(x0 - h)) / 2h| ~~ |(h²/6) * f'''(x0)| = d_err
# d_err = |(h²/6) * -cos(x0)|
d_err = abs(-cos(x0) .* (h .^ 2) / 6);

loglog(h, err, '-*');
hold on
loglog(h, d_err, 'r-.');
xlabel('h')
ylabel('Absolute error')

#{
COMENTÁRIOS:

Ambos os gráficos possuem erros de arredondamento que tomam conta do seu comportamento:
o da figura 1.3 começa a ocorrer quando h é próximo de 1e-08, enquanto o desse
exercício aparece com h próximo a 1e-06. O gráfico desse exercício também não 
possui erro de discretização tão evidente como o da figura 1.3, entretanto possui 
um erro de arredondamento muito maior, devido principalmente a característica das
operações matemáticas feitas nesse exercício.
#}
