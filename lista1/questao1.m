# f(x) = e^(-2x)  =>  f'(x) = -2e^(-2x)  =>  f''(x) = 4e^(-2x)
x0 = 0.5;
f0 = e^(-2 * x0);
fp = -2 * e^(-2 * x0);

i = -20:0.5:0;
h = 10.^i

err = abs(fp - (e.^(-2 .* (x0 + h)) - f0) ./ h)

# [f'(x0) - (f(x0 + h) - f(x0)) / h] ~~ (h/2) * f''(x0) = d_err
# d_err = 2 * h * e^(-2x)
d_err = 2*h*f0

loglog(h, err, '-*');
hold on
loglog(h, d_err, 'r-.');
xlabel('h')
ylabel('Absolute error')

#{
COMENTÁRIOS:

Como similaridades com o gráfico da figura 1.3, vemos que a partir de h = 10^-8
os erros de arredondamento afetam claramente o comportamento do gráfico.

Como diferenças, podemos perceber que esse gráfico não possui o erro de
discretização tão evidente como no gráfico da figura 1.3 quando h é próximo de h = 10^-8.
#}
