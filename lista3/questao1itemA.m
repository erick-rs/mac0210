increment = 0.00098758;
x_values = 1.921:increment:2.08;
n = 9;

function results = horner_rule(n, x_values)
  results = [];
  c = [-512 2304 -4608 5376 -4032 2016 -672 144 -18 1];
  
  for x=x_values
    p = c(n + 1);

    for i=n:-1:1
      p = p * x + c(i);
    endfor
    
    results = [results p];
  endfor
endfunction

function results = original_rule(n, x_values)
  results = [];
  
  for x=x_values
    px = (x - 2)^n;
    results = [results px];
  endfor
endfunction

figure(1);
plot(x_values, horner_rule(n, x_values));
xlabel('x');
ylabel('p(x)');
title('Cálculo de p(x) pela regra de Horner');

figure(2);
plot(x_values, original_rule(n, x_values));
xlabel('x');
ylabel('p(x)');
title('Cálculo de p(x) normal');


#{
COMENTÁRIOS:

É visível as diferenças entre os dois gráficos. Enquanto que, calculando p(x) por
(x - 2)⁹, o gráfico descreve uma curva de uma função comum, o cálculo por Horner
acarretou em uma série de variações na sua curva, com alguns valores de p(x) no
intervalo [1.921 2.08] divergindo com relação a implementação comum. Muito
provavelmente essas diferenças ocorreram por erros associados as operações aritméticas
realizadas no método de Horner, que acumula erros de multiplicação e de somas ao
longo do processo do calculo de p(x).
#}
