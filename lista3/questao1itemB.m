1;

function y = f_horner(x)
  n = 9;
  c = [-512 2304 -4608 5376 -4032 2016 -672 144 -18 1];
  y = c(n + 1);

  for i=n:-1:1
    y = y * x + c(i);
  endfor
endfunction

function y = f_classic(x)
  y = (x - 2)^9;
endfunction

atol = 1e-6;
a = 1.921;
b = 2.08;

disp('BISECT para a regra de horner');
bisect(@f_horner, a, b, f_horner(a), f_horner(b), atol)
disp('BISECT para o método original');
bisect(@f_classic, a, b, f_classic(a), f_classic(b), atol)

%{
COMENTÁRIOS:

Como vimos no item A, as duas implementações divergiram nos valores de p(x)
em alguns pontos do intervalo dado. Isso refletiu no cálculo da raíz da função,
em que, por horner, o valor encontrado foi 1.9766 e pelo método tradicional foi
2.0005. Explicando mais sucintamente:
O método bisect iniciou com a=1.921 e b=2.08. Então calculou (a+b)/2 = 2.005.
Para a implementação de horner, temos que p(2.005) ~ 4.66e-12, que não é menor que
a constante eps do octave. Por isso, o método continuou por mas iterações.
Já na implementação original, temos que p(2.005) ~ 1.9531e-30, que é menor que eps.
Assim, em uma única iteração, o método já encontrou uma raíz.
%}
