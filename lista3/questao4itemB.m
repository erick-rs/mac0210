2;

function [x, n] = fixedpoint(f, a, x0, atol)
  x = f(x0, a);
  n = 1;
  while (abs(x - x0) > atol)
    x0 = x;
    x = f(x0, a);
    n += 1;
  endwhile
endfunction

function y = g(x, a)
  y = x - x^3 + a;
endfunction

function [x, n] = curt2(a, x0, atol)
  [x, n] = fixedpoint(@g, a, x0, atol);
endfunction


%{
Sendo g(x) = x - x³ - a, temos que g'(x) = 1 - 3x².
Queremos |g'(x)| < rho < 1, para x > 0. Um intervalo estimado de valores de x
que satisfazem essa condição é [0, 0.813].
Analisando agora a função g(x) nesse intervalo estimado, vemos que valores de a possíveis
são [0, 0.53] mais ou menos.
%}
