2;

function [p, n] = bisseccao(func, constant, a, b, fa, fb, atol)
  if (a >= b) || (fa*fb >= 0) || (atol <= 0)
    disp(' something wrong with the input: quitting');
    p = NaN; n=NaN;
    return
  endif

  n = ceil ( log2 (b-a) - log2 (2*atol));
  for k=1:n
    p = (a+b)/2;
    fp = func(p, constant);
    # obs: a linha abaixo não existe na 1a edição do livro
    if (abs(fp) < eps)
      n = k;
      return;
    endif
    if fa * fp < 0
      b = p;
    else
      a = p;
      fa = fp;
    endif
  endfor

  p = (a+b)/2; 
endfunction

function y = f(x, a)
  y = x^3 - a;  
endfunction

function [p, n] = curt1(a, atol)
  if (0 < a <= 1)
    start_at = a;
    end_at = 1;
  else
    start_at = 1;
    end_at = a;
  endif
  
  [p, n] = bisseccao(@f, a, start_at, end_at, f(start_at, a), f(end_at, a), atol);
endfunction

curt1(0.6, 1e-8)

%{
  Sabemos que:
  |x_k - x*| <= (e - s)/2 * (1/2)^k

  Onde [s, e] é o intervalo que estamos considerando.

  Limitando por atol (tolerância):

  |x_k - x*| <= (e - s)/2 * (1/2)^k < atol

  =>  (e - s)/2 * (1/2)^k < atol
  =>  log_2((e-s)/2 * (1/2)^k) < log_2(atol)
  =>  log_2((e-s)/2) + (log_2(1^k) - log_2(2^k)) < log_2(atol)
  =>  log_2((e-s)/2) + (0 - k) < log_2(atol)
  =>  k > log_2((e-s)/2) - log_2(atol)
  =>  k > log_2((e-s)/2*atol)

  No intervalo [1, a]: k > log_2((a - 1)/2*atol)
  No intervalo [a, 1]: k > log_2((1 - a)/2*atol)

  Assim: k > log_2(|a - 1|/2*atol)
%}

