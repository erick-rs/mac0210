2;

function y = f(x, a)
  y = x^3 - a;
endfunction

function y = df(x, a)
  y = 3 * (x^2);  
endfunction

function [x, n] = curt3(a, atol)
  x = (a + 2) / 3;
  n = 1;

  do
    d = df(x, a);
    
    if (abs(d) < eps)
      disp("Derivada é igual a 0");
      return;
    endif
    
    x = x - f(x, a) / d;
    n += 1;
  until (abs(x^3 - a) <= atol)
endfunction
