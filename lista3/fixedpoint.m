function x = fixedpoint(f, x0, atol)
  x = f(x0)
  while (abs(x - x0) > atol)
    x0 = x;
    x = f(x0)
  endwhile
endfunction
