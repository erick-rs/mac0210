2;

x = [0, 1, 3];
y = [1, 0.9, 0.5];

i = 0:0.01:6;
z = log(y);

c0 = z(1);
c1 = (z(2) - z(1)) / (x(2) - x(1));
c2 = (z(3) - z(1) - c1*(x(3) - x(1))) / ((x(3) - x(1)) * (x(3) - x(2)));

gamma0 = e^(x(1) - c1*x(1) + c2*x(1)*x(2))
gamma1 = c1 - c2*(x(2) + x(1))
gamma2 = c2

ux = gamma0.*e.^(gamma1.*i + gamma2.*i.^2);

figure(1);
plot(i, ux, 'b', x(1), y(1), 'r*',  x(2), y(2), 'r*',  x(3), y(3), 'r*');
legend("u(x)", "pontos observados");
title('u(x) no intervalo [0,6]');
xlabel('x');
ylabel('u(x)');

#{
O gráfico da função não se parece com o gráfico de uma função quadrática. No intervalo
[0,6], o gráfico é semelhante ao gráfico de uma função do primeiro grau. Podemos 
perceber também que, como u(x) é uma função exponencial, não possui raízes, ao contrário
de uma função quadrática convencional em que delta >= 0.
Como última observação, se utilizarmos um intervalo maior como por exemplo [-10, 10],
o gráfico é semelhante ao de uma gaussiana. 
#}

