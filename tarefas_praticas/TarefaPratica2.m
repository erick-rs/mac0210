#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Você deve submeter esse arquivo com esse mesmo nome 'quadsolver.m', no qual as primeiras linhas devem ser o cabeçalho preenchido e a definição da função.

Nome : Erick Rodrigues de Santana
NUSP : 11222008
Matéria: MAC0210
Turma: 2021
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 2 é escrever em Octave um resolvedor genérico de equações quadráticas da forma ax²+bx+c = 0 que seja numericamente robusto. Sua função deve ter a forma

s = quadsolver(a,b,c)

onde os três parâmetros a, b, c são números reais em ponto flutuante e s é o vetor com todas as soluções reais e/ou complexas. Se a equação não admitir solução, sua função deve devolver s = [], e se existirem infinitas soluções, sua função deve devolver o código especial s = "R".

Você pode usar as equações de Bháskara s₁,₂ = (-b±sqrt(Δ))/(2a) com Δ=b²-4ac, bem como a relação s₁s₂=c, conforme a necessidade. Tenha cuidado para evitar os erros de cancelamento (quando b≈±sqrt(Δ)) e de overflow (quando b² ou b/(2a) extrapolam o limite representável de 10³⁰⁸), como explicado no exemplo 2.10, e lembre de corrigir as soluções para s₁,₂ = (-b±i*sqrt(|Δ|))/(2a) quando Δ<0. 

Alguns casos importantes de teste seguem abaixo; você pode usar o comando roots([a b c]) para reconfirmar as raízes esperadas (a única diferença relevante ocorre no caso A, em que a resposta deve ser "R"):

A) quadsolver(0, 0, 0)
B) quadsolver(0, 0, 1)
C) quadsolver(0, 10^-300, 1)
D) quadsolver(10^-150, -10^150, 10^150)
E) quadsolver(10^-300, -10^300, 10^300)
F) quadsolver(1, -10^30, 1)
G) quadsolver(6*10^300, 5*10^300, -4*10^300)
H) quadsolver(10^300, 0, 10^300)

#}

function s = quadsolver(a, b, c)
  x1 = 0;
  x2 = 0;

  if (a && b && c && abs(log10(abs(b))) + abs(log10(abs(2*a))) > 308)
    m = max([a b c]);
    a = a/m;
    b = b/m;
    c = c/m;
  endif

  if (a != 0)
    delta = b^2 - 4 * a * c;
    qtde_digitos_a = 0;
    qtde_digitos_b = 0;
    qtde_digitos_c = 0;

    # Pegar quantidade de digitos de cada coeficiente para ver se vai estourar ou não
    if (a != 0)
      qtde_digitos_a = log10(abs(a));
    endif
    if (b != 0)
      qtde_digitos_b = log10(abs(b));
    endif
    if (c != 0)
      qtde_digitos_c = log10(abs(c));
    endif

    if (abs(qtde_digitos_b) > 150)
      #{
        Caso em que o B² estoura: simplificamos sqrt(b² - 4ac) removendo o b². Então:
        sqrt(b² - 4ac) = sqrt(b² * (1 - (4 * a* c) / b²)) = sqrt(b²) * sqrt(1 - 4 * (a/b) * (c/b)) = abs(b) * sqrt(1 - 4 * (a/b) * (c/b))
        Dessa forma, temos um underflow
      #}
      raiz_delta = abs(b) * sqrt(1 - 4 * (a / b) * (c / b));
    elseif (abs(qtde_digitos_a) > 150 && abs(qtde_digitos_c) > 150)
      #{
        Caso em que a * c estoura: simplificamos sqrt(b² - 4ac) colocando o termo 4ac como denominador. Então:
        sqrt(b² - 4ac) = sqrt(4ac * (b²/4ac - 1)) = sqrt(4ac) * sqrt(b²/4ac - 1) = 2 * sqrt(a) * sqrt(c) * sqrt(abs(b²/4ac - 1))
        Dessa forma, temos um underflow
      #}
      raiz_delta = 2 * sqrt(a) * sqrt(c) * sqrt(abs((b^2) / (4 * a * c) - 1));
    else
      # Caso safe, faz tira a raiz quadrada do delta normal
      raiz_delta = sqrt(abs(delta));
    endif

    #{
      Podemos alterar a fórmula de Bháskara para x = 2c / (-b +- sqrt(delta)). Observe:
      x = (-b +- sqrt(delta)) / 2a
        = [(-b +- sqrt(delta)) / 2a] * [(-b -+ sqrt(delta)) / (-b -+ sqrt(delta))]   -> Multiplicamos pelo conjugado
        = 2c / (-b +- sqrt(delta))

      Agora temos o delta no denominador e nos casos particulares desse exercício, essa fórmula é mais estável
    #}

    if (delta == 0) # Única solução
      x1 = -b / (2 * a);
      x2 = x1;
    elseif (raiz_delta == abs(b)) # Caso do erro de cancelamento
      #{
        Truque desse caso: calcular uma das soluções de forma que (-b +- raiz_delta) não zere.
        Após ter uma solução, calcular a outra via o produto x1 * x2 = c/a 
      #}
      if (b > 0)
        if (delta > 0)
          x2 = (-b - raiz_delta) / (2 * a);
        else
          x2 = (2 * c) / (-b - i * raiz_delta);
        endif

        x1 = c / (x2 * a);
      else
        if (delta > 0)
          x1 = (2 * c) / (-b + raiz_delta);
        else
          x1 = (2 * c) / (-b + i * raiz_delta);
        endif

        x2 = c / (x1 * a);
      endif
    else
      # Caso padrão.
      if (delta > 0)
        x1 = (2 * c) / (-b + raiz_delta);
        x2 = (2 * c) / (-b - raiz_delta);
      else
        x1 = (2 * c) / (-b + i * raiz_delta);
        x2 = (2 * c) / (-b - i * raiz_delta);
      endif
    endif

    s = [x1 x2]';
  else
    # a == 0
    if (b == 0)
      if (c == 0)
        # Caso em que temos a igualdade 0 = 0. Qualquer solução é possível
        s = "R"; 
      else
        # Caso em que temos c = 0, com c != 0. Não existe solução
        s = []';
      endif
    else
      # Caso em que temos a equação bx + c = 0. Nesse caso x = -c / b;
      s = -c / b;
    endif
  endif
endfunction

display("A) a = 0, b = 0, c = 0")
func_roots = roots([0 0 0])
func_quadsolver = quadsolver(0, 0, 0)
printf("\n")

display("B) a = 0, b = 0, c = 1")
func_roots = roots([0 0 1])
func_quadsolver = quadsolver(0, 0, 1)
printf("\n")

display("C) a = 0, b = 10^-300, c = 1")
func_roots = roots([0 10^-300 1])
func_quadsolver = quadsolver(0, 10^-300, 1)
printf("\n")

display("D) a = 10^-150, b = -10^150, c = 10^150")
func_roots = roots([10^-150 -10^150 10^150])
func_quadsolver = quadsolver(10^-150, -10^150, 10^150)
printf("\n")

display("E) a = 10^-300, b = -10^300, c = 10^300")
func_roots = roots([10^-300 -10^300 10^300])
func_quadsolver = quadsolver(10^-300, -10^300, 10^300)
printf("\n")

display("F) a = 1, b = -10^30, c = 1")
func_roots = roots([1 -10^30 1])
func_quadsolver = quadsolver(1, -10^30, 1)
printf("\n")

display("G) a = 6*10^300, b = 5*10^300, c = -4*10^300")
func_roots = roots([6*10^300 5*10^300 -4*10^300])
func_quadsolver = quadsolver(6*10^300, 5*10^300, -4*10^300)
printf("\n")

display("H) a = 10^300, b = 0, c = 10^300")
func_roots = roots([10^300 0 10^300])
func_quadsolver = quadsolver(10^300, 0, 10^300)
printf("\n")

% roots([10^300, 10^300, 10^300])
% quadsolver(10^300, 10^300, 10^300)
% roots([10^-300, 10^300, 10^300])
% quadsolver(10^-300, 10^300, 10^300)
% roots([10^300, 10^-300, 10^300])
% quadsolver(10^300, 10^-300, 10^300)
% roots([10^-300, 10^-300, 10^300])
% quadsolver(10^-300, 10^-300, 10^300)
% roots([10^300, 10^300, 10^-300])
% quadsolver(10^300, 10^300, 10^-300)
% roots([10^-300, 10^300, 10^-300])
% quadsolver(10^-300, 10^300, 10^-300)
% roots([10^300, 10^-300, 10^-300])
% quadsolver(10^300, 10^-300, 10^-300)
% roots([10^-300, 10^-300, 10^-300])
% quadsolver(10^-300, 10^-300, 10^-300)
