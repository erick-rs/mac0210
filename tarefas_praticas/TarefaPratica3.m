#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Nome : Erick Rodrigues de Santana
NUSP : 11222008
Matéria: MAC0210
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 3 é explorar a aplicação do Método de Newton ao problema de minimizar uma função f(x) genérica. Como visto em aula, todo mínimo local x* de f(·) é um ponto crítico, ou seja, satisfaz f'(x*)=0. Além disso, uma condição suficiente para que um ponto crítico seja mínimo é satisfazer f"(x*)>0. A tarefa está organizada em 4 itens, descritos abaixo.

#}

0; # força o Octave a interpretar esse arquivo como um script file (ver https://octave.org/doc/v5.2.0/Script-Files.html)

#{

Item 1: Implementação do método (feito em aula)

Escreva uma função Minimiza que recebe uma função f() e duas derivadas df()=f'() e ddf()=f"(), um ponto inicial x0 e uma tolerância atol, e aplica o método de Newton para a equação f'(x)=0:

x ← x-f'(x)/f"(x)

interrompendo a sequência quando a diferença absoluta entre iterados sucessivos for menor do que atol (critério de convergência), ou quando a próxima iteração do método exigisse uma divisão por zero.

Sua função deve devolver:
x = último iterado
n = número de iterações realizadas
v = valor de função obtido
t = tipo de terminação (t=1 indica que x é ponto de mínimo, t=-1 indica máximo, t=0 indica outro tipo de ponto crítico, e t=NaN indica que o método foi interrompido antes do critério de convergência).

#}

function [x, n, v, t] = Minimiza(f, df, ddf, x0, atol)
  n = 0;
  do
    if abs(ddf(x0)) < eps
      x = x0;
      v = f(x);
      t = NaN;
      return;
    endif
    x = x0 - df(x0) / ddf(x0);
    n = n + 1;
    diferenca = abs(x - x0);
    x0 = x;
  until diferenca < atol;
  v = f(x);
  t = sign(ddf(x));
endfunction


#{

Item 2: Teste do método

Execute sua função nos casos de teste abaixo, e comente no código os resultados. Em particular, explique sucintamente o que ocorreu nos casos em que o método NÃO encontrou um ponto de mínimo.

#}

disp("ITEM 2");
disp("-------------------------");
## exemplo de funções de teste
function y=f(x)   y=sin(x);  end
function y=df(x)  y=cos(x);  end
function y=ddf(x) y=-sin(x); end

## códigos de teste
[x,n,v,t] = Minimiza(@f,@df,@ddf,0,1e-8)
[x,n,v,t] = Minimiza(@f,@df,@ddf,0.1,1e-8)
[x,n,v,t] = Minimiza(@f,@df,@ddf,1,1e-8)
[x,n,v,t] = Minimiza(@f,@df,@ddf,5,1e-8)

disp("-------------------------");
#{
Comentários:

Teste 1: Minimiza(@f,@df,@ddf,0,1e-8)
Esse caso, temos que a segunda derivada (-sin(x)), para x = 0 vale 0. Assim, não conseguímos determinar se x é
um ponto de mínimo, de inflexão ou um ponto de máximo. Na prática, verificamos na função Minimiza se o valor absoluto
da segunda derivada de f é menor que a constante eps definida no octave.

Teste 2: Minimiza(@f,@df,@ddf,0.1,1e-8)
Aqui o método é executado com sucesso. O primeiro valor de x calculado pelo método de Newton vale 10.067, uma diferença
absoluta bem grande comparado ao valor inicial. Depois de 6 passos, o método vai aproximando o valor de x até chegar no valor
10.996, um ponto de mínimo local no qual f(x) = -1 e f'(x) = 0. Entretanto, esse ponto encontrado não é o ponto de mínimo mais
próximo de x0 = 0.1. O método deu um passo muito grande, mas ainda sim encontrou um ponto.

Teste 3: [x,n,v,t] = Minimiza(@f,@df,@ddf,1,1e-8)
Aqui a função Minimiza encontra um ponto de máximo x = 1.5708. Isso ocorre pois, a partir de x0 = 1, o próximo valor de x
é incrementado pois df/ddf resulta em um número positivo. Isso resulta em x = 1.6421. A partir daí, os valores de x vão diminuindo até 
que f'(x) = 0, que por coincidência é o um ponto de máximo.

Teste 4: [x,n,v,t] = Minimiza(@f,@df,@ddf,5,1e-8)
A função é executada com sucesso, obtendo um ponto de mínimo, bem próximo do valor x0 = 5.
#}


#{

Item 3: Melhorando a convergência do método

Uma forma de evitar as situações encontradas no item 2 consiste em interceptar as iterações do método de Newton em que f"(x)<0, que poderiam levar a sequência a um ponto pior, e mudar a definição do método substituindo f"(x) por 1 no denominador:

x ← x-f'(x)

Essa iteração está associada ao Método de Cauchy ou Método da Descida do Gradiente. A mesma iteração poderia ser usada quando f"(x)≈0, um caso em que o método de Newton não está definido.

Re-escreva o método do Item 1 com essas duas modificações e execute-o nos exemplos de teste do Item 2, comentando os resultados obtidos.

#}

function [x, n, v, t] = Minimiza2(f, df, ddf, x0, atol)
  n = 0;
  do
    if (ddf(x0) < 0 || abs(ddf(x0)) < eps)
      x = x0 - df(x0);
    else
      x = x0 - df(x0) / ddf(x0);
    endif
    
    n = n + 1;
    diferenca = abs(x - x0);
    x0 = x;
  until diferenca < atol;
  v = f(x);
  t = sign(ddf(x));
endfunction

disp("ITEM 3");
disp("-------------------------");

## códigos de teste
[x,n,v,t] = Minimiza2(@f,@df,@ddf,0,1e-8)
[x,n,v,t] = Minimiza2(@f,@df,@ddf,0.1,1e-8)
[x,n,v,t] = Minimiza2(@f,@df,@ddf,1,1e-8)
[x,n,v,t] = Minimiza2(@f,@df,@ddf,5,1e-8)

disp("-------------------------");

#{
Comentários:

Com a condição imposta nessa nova função, resolvemos o problema que ocorria no primeiro teste onde x0 = 0. Antes
a segunda derivada era zero e a função retornava t=Nan. Agora, a função retorna o ponto de mínimo x = -1.5708.
No segundo exemplo, onde x0 = 0.1, a função retorna x = -1.5708, que é um ponto de mínimo válido. A diferença para
a implementação anterior é que a função não dá um salto muito grande nos valores de x entre um passo e outro: a primeira
implementação ia de x = 0.1 para x = 10.067 e encontrava x = 10.996.
O terceiro exemplo retornava um ponto de máximo anteriormente. Agora, ela retorna um ponto de mínimo, com valor igual aos 2 testes
de x0 = 0 e x0 = 0.1.
Por fim, o último exemplo se manteve igual com relação aos resultados quanto aos valores de x, t e v.
#}

#{

Item 4: Método Quasi-Newton

Frequentemente a necessidade de conhecer as expressões analíticas das derivadas de f() pode ser um empecilho à aplicação do método de Newton. Uma alternativa é usar aproximações, tais como

f'(x) ≈ (f(x+h)-f(x-h)) / (2h)
f"(x) ≈ (f'(x+h)-f'(x-h)) / (2h) ≈ (f(x+2h)-2*f(x)+f(x-2h)) / (4h²)

para algum h≠0 razoável. Isso poderia ser implementado no método de Newton usando h=x_atual-x_anterior. Para isso seu método precisa de 2 pontos iniciais.

Adapte sua função do item 3 usando as aproximações das derivadas acima, e teste-a nos casos indicados, comentando os resultados: o método falhou em algum caso? Como o número de iterações variou em relação ao item 3?

#}

#{
  h = x - x0
x0 + h = x0 + x - x0 = x
x0 - h = x0 - (x - x0) = (2x0 - x)

x0 + 2h = x0 + 2(x - x0) = 2x - x0
x0 - 2h = x0 - 2(x - x0) = 3x0 - 2x 
#}

function [x, n, v, t] = Minimiza3(f, x0, x1, atol)
  n = 0;
  h = x1 - x0;
  x0 = x1;
  
  do
    df = (f(x0 + h) - f(x0 - h)) / (2*h);
    ddf = (f(x0 + 2*h) - 2*f(x0) + f(x0 - 2*h)) / (4*h^2);
    if (ddf < 0 || abs(ddf) < eps)
      x = x0 - df;
    else
      x = x0 - df / ddf;
    endif
    
    diferenca = abs(x - x0);
    h = x - x0;
    x0 = x;
    n = n + 1;
  until diferenca < atol;
  v = f(x);
  t = sign(ddf);
endfunction

disp("ITEM 4");
disp("-------------------------");

## códigos de teste
[x,n,v,t] = Minimiza3(@f,-0.1,0,1e-8)
[x,n,v,t] = Minimiza3(@f,0,0.1,1e-8)
[x,n,v,t] = Minimiza3(@f,0.5,1,1e-8)
[x,n,v,t] = Minimiza3(@f,4,5,1e-8)

disp("-------------------------");
#{

Comentários:

Observa-se que, assim como a implementação do item 3, não ocorreu nenhuma falha, conseguindo achar um ponto de 
mínimo válido. Porém, uma diferença gritante que ocorre é quanto ao número de passos que a função levou para encontrar
a solução. Em comparação com a implementação do item 3, a quantidade de passos aumentou consideravelmente. Como por
exemplo no terceiro teste com x0 = 0.5 e x1 = 1, essa nova função levou 11 passos, enquanto a implementação anterior levou 8 passos.
Em todos os outros testes, teve um aumento no número de passos. Mesmo assim, todos os testes produziram os mesmos resultados
de x, t, v que no item anterior

#}
