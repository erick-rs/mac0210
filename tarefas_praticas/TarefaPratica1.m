
# Identifique-se:
disp("Tarefa Prática 1")
disp("Nome: Erick Rodrigues de Santana")
disp("NUSP: 11222008")

#{ 
1) escreva uma função myrand(M) com um gerador de números aleatórios positivos
em diversas ordens de grandeza: para isso use as funções rand() para gerar 
valores entre [0,1] e randi([-M,M]) para gerar expoentes (em base 10). 
Você pode usar M=10 no restante desta tarefa.
#}

M = 10;

function x = myrand(M)
  x = rand() * 10^randi([-M M]);
endfunction

#{
2) escreva um par de funções xenc = enc(x,N) e x = dec(xenc,N) para 
codificar/decodificar um número fracionário positivo x em uma representação 
xenc=(m,e), usando uma variável inteira m com N dígitos decimais significativos
(alinhados à esquerda, ou seja, com o dígito associado a 10^(N-1) sempre 
não-nulo) e um expoente e em base 10. Por exemplo, usando N=5, o valor x=123
seria codificado como m=12300 e e=-2 (pois x=12300*10^(-2)), enquanto o valor 
x=1.23456 seria codificado como m=12346 e e=-4 (pois x~12346*10^(-4), sendo o 
último dígito o resultado de um arredondamento). Teste sua função com alguns 
valores gerados pela função myrand(), copiando as saídas em comentários no 
código.
#}

function xenc = enc(x, N)
  e = floor(log10(x)) + 1 - N;
  m = round(x * 10^(-e)); # x * 10 ^ (N - 1 - floor(log10(x)));
  xenc = [m e];
endfunction

function x = dec(xenc, N)
  x = xenc(1) * 10^xenc(2);
endfunction

#{
Testes do item 2)

Com M=10 e N=5:

x =  0.00085606
enc = [85606      -8]
dec = 0.00085606

x =  0.000000000051721
enc = [51721     -15]
dec = 0.000000000051721

x =  29.248
enc = [29248      -3]
dec = 29.248

x = 9296.6
enc = [92966      -1]
dec = 9296.6

x =  0.0000000073429
enc = [73429     -13]
dec = 0.0000000073429 
#}

#{
3) escreva uma função encdec(x) para codificar e decodificar um valor x, 
devolvendo o erro absoluto e o erro relativo produzidos no processo. Selecione 
três valores aleatórios em ordens de grandeza diferentes (considere valores 
"pequenos" se e≤-6, "médios" se -2≤e≤2 e "grandes" se e≥+6) e imprima o erro 
absoluto e o erro relativo do processo de codificação+decodificação. 
Você percebe um padrão? Comente no código.
#}

function err = encdec(x, N)
  xenc = enc(x, N);
  y = dec(xenc, N);
  
  err_abs = abs(x - y);
  err_rel = err_abs / abs(x);
  err = [err_abs, err_rel];
endfunction

#{
Exemplos com N = 5

Pequeno: 0.000000038328
Resultado = [0 0]

Médio: 7328559.98236
Resultado = [40.0176400002    0.0000054605]

Grande: 18582821481.37942
Resultado = [178518.6205787659        0.0000096066]


Com valores pequenos, os erros tendem a ser bem pequenos, em especial o absoluto.
Isso ocorre pois é possível representar a mantissa com o valor de N utilizado.
Agora para valores de x muito grandes, o erro absoluto cresce, mas o erro relativo
continua pequeno.

#}

#{
4) faça uma função testaencdec(N) que repita o experimento do item anterior 1000
 vezes, plotando os gráficos dos erros absolutos e relativos, e execute-a para 
 N=3, 5 e 10. Quais os valores máximos observados em cada caso? Você saberia 
 explicá-los em função de M e N? Comente no código.
#}

function testaencdec(N)
  eixo_x = [];
  erros = [];
  for i=1:1000
    x = myrand(10);
    err = encdec(x, N);
    eixo_x = [eixo_x x];
    erros = [erros; err];
  endfor
 
  figure(1);
  loglog(eixo_x, erros(:, 1), '.', eixo_x, erros(:, 2), '.');
  xlabel("Valores");
  ylabel("Erros");
  title("Erros absolutos e relativos");
endfunction

#{
Resultados aproximados:

N = 3:
max err_abs = 4.7e+6
max err_rel = 0.0045

N = 5:
max err_abs = 47500
max err_rel = 4.47e-05

N = 10:
max err_abs = 0.49
max err_rel = 4.6e-10

Ao aumentar o N, os erros absolutos e relativos vão diminuindo. Mais precisamente,
quando temos N e M quase que iguais, temos um erro próximo a zero, já que a mantissa
tem tamanho suficiente para armazenar os números. Entretanto, quando N é muito menor
que o M, temos um arredondamento maior pra caber na mantissa.
#}



#{
5) implemente uma função soma(x,y,N) que receba dois números xenc=(mx,ex) e 
yenc=(my,ey), codificados com N dígitos significativos, e que produza um 
resultado zenc=(mz,ez), também codificado. Para isso, decodifique as entradas, 
aplique a soma usual em octave e codifique o resultado. Teste sua função com 
alguns pares (x,y) de valores aleatórios, e selecione 3 exemplos onde (x,y) 
sejam respectivamente (pequeno,pequeno), (pequeno,grande) e (grande,grande), 
registrando-os como comentários no código.
#}

function sum = soma(x, y, N)
  decod_x = dec(x, N);
  decod_y = dec(y, N);
  soma = decod_x + decod_y;
  sum = enc(soma, N);
endfunction

#{
(pequeno pequeno) = (0.000000000062427, 0.020279)
Resultado = [20279      -6]

(pequeno grande) = (7.3606, 401911.89842)
Resultado = [40192       1]

(grande grande) = (43889493.92385, 6026915825.07855)
Resultado = [60708       5]
#}

#{
6) faça uma função testasoma(N) que produza 1000 somas de pares (x,y) de valores
 aleatórios, e plote os erros absolutos e relativos considerando dois 
 "ground truths" diferentes: (1) a soma x+y realizada em octave, e (2) a 
 expressão encdec(x)+encdec(y). Faça isso para N=3, 5 e 10. Quais os valores 
 máximos observados em cada caso? Qual a interpretação dos dois
 "ground truths"? Comente no código.
#}

function testasoma(N)
  somas_verdadeira = [];
  somas_funcsoma = [];
  somas_encdec = [];

  for i=1:1000
    x = myrand(10);
    y = myrand(10);
    z = dec(soma(enc(x, N), enc(y, N), N), N);
    w = dec(enc(x, N), N) + dec(enc(y, N), N);
    somas_verdadeira = [somas_verdadeira (x+y)];
    somas_funcsoma = [somas_funcsoma z];
    somas_encdec = [somas_encdec w];
  endfor

  erros_abs1 = abs(somas_verdadeira - somas_funcsoma);
  erros_abs2 = abs(somas_encdec - somas_funcsoma);
  erros_rel1 = erros_abs1 ./ abs(somas_verdadeira);
  erros_rel2 = erros_abs2 ./ abs(somas_funcsoma);

  figure(1);
  loglog(somas_verdadeira, erros_abs1, '.', somas_verdadeira, erros_rel1, '.');
  xlabel("Valores da soma reais");
  ylabel("Erros");
  title("Erros absolutos e relativos - Ground Truth 1");

  figure(2);
  loglog(somas_verdadeira, erros_abs2, '.', somas_verdadeira, erros_rel2, '.');
  xlabel("Valores da soma reais");
  ylabel("Erros");
  title("Erros absolutos e relativos - Ground Truth 2");
endfunction

#{
N=3

MAX GT1 = 4.6e+07
MX GT2 = 4.9e+07

N=5

MAX GT1 = 430000
MAX GT2 = 500000

N=10

MAX GT1 = 2.6
MAX GT2 = 3

Explicação: Estamos propagando o erro, dependendo da fonte de verdade escolhida.
No primeiro ground truth, estamos pegando o valor real e comparando com os resultados
da função soma, que tem um certo erro. Já no segundo ground truth, estamos usando os resultados
da função soma como fonte de verdade, que já possui erros. Realizando as operações
enc dec na mão, temos também um erro que, quando comparado com os da função soma, pode ser maior
ou menor que o erro do primeiro ground truth (dependendo dos valores de x e y gerados)´

#}

