x = 10.^(1:20);

exp1 = log(x - sqrt(x.^2 - 1));
exp2 = -log(x + sqrt(x.^2 - 1));

[exp1; exp2]'

loglog(x, exp1, '-*', x, exp2, 'r-.');
xlabel('x')
ylabel('f(x)')
legend('ln(x - sqrt(x^2 - 1))', '-ln(x + sqrt(x^2 - 1))');

#{
A melhor formula para representar seria a expressão 2 [-log(x + sqrt(x.^2 - 1))].
Podemos ver pelos valores gerados que, a partir de 10^8, a primeira expressão não
consegue representar mais os valores (começa a resultar em -inf). Isso acontece
devido ao erro de cancelamento que ocorre na expressão 1: para valores muito grandes
de x, o termo sqrt(x^2 - 1) vale aproximadamente sqrt(x^2) que, como x > 0, resulta
em aproximadamente x. Assim, temos aproximadamente a expressão log(x - x) = log(0) = -inf.
A expressão 2 evita esse erro de cancelamento, pois dentro do log temos uma soma ao invés
de uma subtração.
#}
