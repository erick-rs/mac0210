format long e;
sum1 = 0;
N = 10000;

function xenc = enc(x, N)
  e = floor(log10(x)) + 1 - N;
  m = round(x * 10^(-e)); # x * 10 ^ (N - 1 - floor(log10(x)));
  xenc = [m e];
endfunction

function x = dec(xenc, N)
  x = xenc(1) * 10^xenc(2);
endfunction

# Implementação I: forma padrão

for i=1:N
  sum1 = sum1 + 1 / i;
endfor

# Implementação II: com arredondamento de 5 casas decimais usando enc e dec
digits = 5;

sum2 = enc(1, digits);
for i=2:N
  sum2 = enc(1/i + dec(sum2, digits), digits);
endfor

sum2 = dec(sum2, digits);

# Implementação III: ordem inversa da II
sum3 = enc(1/N, digits);
for i=N:-1:1
  sum3 = enc(1/i + dec(sum3, digits), digits);
endfor

sum3 = dec(sum3, digits);

display("RESULTADOS:")
display("Implementação 1: ")
sum1
display("Implementação 2: ")
sum2
display("Implementação 3: ")
sum3


#{
COMENTÁRIOS:

Vemos que a implementação 1 produz o resultado coerente já que utiliza a representação
de double padrão do octave (que consegue representar valores até 52 casas decimais).
Nas implementações 2 e 3, utilizamos 5 casas decimais na representação e produzimos
resultados diferentes mudando somente a ordem das operações. Isso acontece porque
quando começamos as operações de números muito pequenos, conseguimos representar muito
melhor as operações feitas sobre os números pequenos, resultando em maior precisão.
Quando começamos as operações com números muito grandes, ao fazer sum + 1/i a cada iteração,
se 1/i for muito pequeno, o sistema vai arredondar (sum + 1/i) para sum, o que diminui a precisão
do resultado final.

#}
