# f(x) = sin(x) => f'(x) = cos(x) => f''(x) = -sin(x)
x0 = 1.2;
f0 = sin(x0);
fp = cos(x0);

i = -20:1:0;
h = 10.^i
fp_aprox = (sin(x0 + h) - sin(x0)) ./ h;

# sin(x0 + h) - sin(x0) = 2 * cos((x0 + h + x0) / 2) * sin((x0 + h - x0) / 2)
# sin(x0 + h) - sin(x0) = 2 * cos(x0 + h/2) * sin(h/2)

nova_aprox = (2 * cos(x0 + h./2) .* sin(h ./ 2)) ./ h 

err = abs(fp - fp_aprox);

erro_novo = abs(fp - nova_aprox);

d_err = abs(h .* (-sin(x0)) / 2);

figure(1)
loglog(h, err, '-*', h, erro_novo, 'g-*', h, d_err, 'r-.');
xlabel('h')
ylabel('Erros absolutos')
legend('Erro da fórmula original', 'Erro usando identidade trigonométrica', 'Erro da aproximação de taylor', 'location', 'southeast');

#{
COMENTÁRIOS:

Com a identidade sin(x0 + h) - sin(x0) = 2 * cos(x0 + h/2) * sin(h/2), resolvemos
o erro de cancelamento que era o mais evidente, já que não realizamos mais a subtração
do lado esquerdo da igualdade, que para valores de h muito pequenos, acabava por
zerar a expressão.
Vale notar que, a partir de 10^-15, o valor obtido pela nova aproximação é praticamente
igual ao valor original, o que acaba zerando o erro absoluto e não mostrando no gráfico.
#}
