format short;

N = 2;
x = 1:20;

# Funções enc e dec pra representar com 2 casas decimais
function xenc = enc(x, N)
  if (x == 0)
    xenc = [0 0];
  else
    e = floor(log10(x)) + 1 - N;
    m = round(x * 10^(-e)); # x * 10 ^ (N - 1 - floor(log10(x)));
    xenc = [m e];
  endif
endfunction

function x = dec(xenc, N)
  x = xenc(1) * 10^xenc(2);
endfunction

variance = var(x);
average = dec(enc(mean(x), N));

var1 = enc(0, N);
var2 = enc(0, N);

for i=1:length(x)
  current = dec(enc(x(i), N), N);
  term_var1 = sign(current - average) * dec(enc(abs(current - average), N), N);
  term_var2 = enc(current^2, N);
  var1 = enc(dec(var1, N) + dec(enc(term_var1^2, N), N), N);
  var2 = enc(dec(var2, N) + dec(term_var2, N), N);
endfor

tam = enc(length(x), N);
var1 = dec(enc(dec(var1, N) / dec(tam, N), N), N);
var2 = enc(dec(var2, N) / dec(tam, N), N);  
var2 = dec(enc(dec(var2, N) - dec(enc(average^2, N), N), N), N);
  
display(["Ground truth: " num2str(variance)]);
display(["Variância calculada pela primeira fórmula: " num2str(var1)]);
display(["Variância calculada pela segunda fórmula: " num2str(var2)]);
